package com.example.farshid.webview;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private Button nextButton, backButton;
    private ImageButton refreshImageButton;
    private ImageView destroyImageView, loading;
    private ProgressBar progressBar;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //assign views to object reference variables

        backButton = (Button) findViewById(R.id.back_button);
        nextButton = (Button) findViewById(R.id.next_button);
        refreshImageButton = (ImageButton) findViewById(R.id.refresh_imageView);
        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        destroyImageView = (ImageView) findViewById(R.id.destroy_imageView);
        loading = (ImageView) findViewById(R.id.imageView9);


        progressBar.setVisibility(View.INVISIBLE);
        webView.getSettings().setJavaScriptEnabled(true);

        myLoadUrl();

        //setting webViewClient to the webView
        webView.setWebViewClient(new MyWebViewClient());


        connectionAvailable();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webView.canGoBack()) {
                    webView.goBack();
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webView.canGoForward()) {
                    webView.goForward();
                }
            }
        });


        refreshImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.reload();

                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.INVISIBLE);
                    refreshImageButton.setVisibility(View.VISIBLE);
                }
            }
        });

        destroyImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.stopLoading();
            }
        });


    }//end on create method


    private boolean connectionAvailable() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }else{
            Toast.makeText(getApplicationContext(),"No Internet Connection!",Toast.LENGTH_LONG).show();
        }
        return connected;
    }


    //this method calls when we want to search
    private void myLoadUrl() {
//        String url = urlEditText.getText().toString();
        //check whether the http protocol inserted by the user or not
//        if(url.contains("https://") || url.contains("http://")) {
//            webView.loadUrl(url);
//        }else{

        String url = "www.herathost.af/hha/";
        webView.loadUrl("http://" + url);

//        }
    }


    private class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            findViewById(R.id.refresh_imageView).setVisibility(View.INVISIBLE);
            findViewById(R.id.imageView9).setVisibility(View.VISIBLE);
            findViewById(R.id.webView).setVisibility(View.INVISIBLE);
            connectionAvailable();

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            findViewById(R.id.progressBar).setVisibility(View.INVISIBLE);
            findViewById(R.id.refresh_imageView).setVisibility(View.VISIBLE);
            findViewById(R.id.imageView9).setVisibility(View.INVISIBLE);
            findViewById(R.id.webView).setVisibility(View.VISIBLE);
            connectionAvailable();
        }
    }
}